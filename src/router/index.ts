import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '',
      components: {
        default: () => import('../views/Home/views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Authentication/views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requiresAuth: false
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import('../views/Pos/views/POSView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/customer',
      name: 'customer',
      components: {
        default: () => import('../views/Customer/views/CustomerView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/inventory',
      name: 'inventory',
      components: {
        default: () => import('../views/Inventory/views/InventoryView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/employee',
      name: 'employee',
      components: {
        default: () => import('../views/Employee/views/EmployeeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/user',
      name: 'user',
      components: {
        default: () => import('../views/User/views/UserView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import('../views/Product/views/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requiresAuth: true
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}

router.beforeEach(async (to, from) => {
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    router.push({ name: 'login' })
  }
})

export default router
