export type DrawerButton = {
  title: string
  value: string
  path: string
  icon?: string
}
