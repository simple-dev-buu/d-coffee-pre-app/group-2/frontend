import { defineStore } from 'pinia'
// import { useLoadingStore } from './loading'
import ProductService from '../services/product'
import { reactive, ref } from 'vue'
import type { Product } from '../types/Product'
export const useProductStore = defineStore('product', () => {
  // const loadingStore = useLoadingStore()
  const productService = new ProductService()
  const items = ref<Product[]>([])
  const dialog = ref(false)
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Image', key: 'image', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Price', key: 'price', sortable: false },
    {
      title: 'Type',
      key: 'type',
      values: (item: any | Product) => item.type.name,
      sortable: false
    },
    { title: 'Action', key: 'action', sortable: false }
  ]
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 1, name: 'Drink' },
    image: 'picturetest.png',
    files: []
  }
  const productItem = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  const currentProduct = ref<Product | null>()
  // const searchProduct = (name: string) => {
  //   const index1 = items.value.findIndex((item) => item.name === name)
  //   // const index2 = products2.value.findIndex((item) => item.name === name)
  //   // const index3 = products3.value.findIndex((item) => item.name === name)
  //   if (index1 < 0) {
  //     currentProduct.value = null
  //   }
  //   currentProduct.value = items.value[index1]
  // }

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  // const save = async () => {
  //   if (productItem.id) {
  //     await productService.update(productItem)
  //     console.log(productItem)
  //   } else {
  //     await productService.save(productItem)
  //     console.log(productItem)
  //   }
  //   closeDialog()
  //   getAll()
  // }

  async function getProduct(id: number) {
    try {
      // loadingStore.doLoad()
      const res = await productService.getProduct(id)
      productItem.value = res.data
      // loadingStore.finish()
    } catch (e: any) {
      // loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  async function save() {
    try {
      // loadingStore.doLoad()
      const product = productItem.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.update(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.save(product)
      }
      await getAll()
      closeDialog()
      // loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      // loadingStore.finish()
    }
  }
  const resetItem = () => {
    productItem.value = JSON.parse(JSON.stringify(initialProduct))
  }
  const getAll = async () => {
    // loadingStore.doLoad()
    items.value = await productService.getAll()
    console.log(items.value)
    // loadingStore.finishLoad()
  }
  const editItem = async (item: Product) => {
    // product.id = item.id
    // productItem = item.name
    // productItem.price = item.price
    // productItem.type = item.type
    // console.log(productItem.type)
    // openDialog()

    if (item.id) {
      await productService.getProduct(item.id)
      openDialog()
      console.log(getProduct(item.id))
    }
  }

  const deleteItem = async (item: Product) => {
    if (item.id) {
      await productService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    productItem,
    dialog,
    currentProduct,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
