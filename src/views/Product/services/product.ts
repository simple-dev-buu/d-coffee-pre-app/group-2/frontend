import type { Product } from '../types/Product'
import http from '@/services/http'

class ProductService {
  public async save(item: Product & { files: File[] }) {
    // await http.post('/products', item)
    const formData = new FormData()
    formData.append('name', item.name)
    formData.append('price', item.price.toString())
    formData.append('type', JSON.stringify(item.type))
    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    return http.post(`/products/${item.id}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public async update(item: Product & { files: File[] }) {
    // await http.patch('/products', item)
    const formData = new FormData()
    formData.append('name', item.name)
    formData.append('price', item.price.toString())
    formData.append('type', JSON.stringify(item.type))
    if (item.files && item.files.length > 0) formData.append('file', item.files[0])
    return http.post('/products', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
  public async getAll() {
    const res = await http.get('/products')
    return res.data
  }
  public async getProductByType(typeId: number) {
    return http.get(`/products/type/` + typeId)
  }
  public async getProduct(id: number) {
    return http.get(`/products/${id}`)
  }
  public async delete(index: number) {
    await http.delete(`/products/${index}`)
  }
}

export default ProductService

// import type { Product } from '@/Product/types/Product'
// import http from '@/services/http'

// function addProduct(product: Product & { files: File[] }) {
//   const formData = new FormData()
//   formData.append('name', product.name)
//   formData.append('price', product.price.toString())
//   formData.append('type', JSON.stringify(product.type))
//   if (product.files && product.files.length > 0) formData.append('file', product.files[0])
//   return http.post('/products', formData, {
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }

// function updateProduct(product: Product & { files: File[] }) {
//   const formData = new FormData()
//   formData.append('name', product.name)
//   formData.append('price', product.price.toString())
//   formData.append('type', JSON.stringify(product.type))
//   if (product.files && product.files.length > 0) formData.append('file', product.files[0])
//   return http.post(`/products/${product.id}`, formData, {
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }

// function delProduct(product: Product) {
//   return http.delete(`/products/${product.id}`)
// }

// function getProduct(id: number) {
//   return http.get(`/products/${id}`)
// }

// function getProductByType(typeId: number) {
//   return http.get(`/products/type/` + typeId)
// }

// function getProducts() {
//   return http.get('/products', {
//     headers: { Authorization: 'Bearer ' + localStorage.getItem('access_token') }
//   })
// }

// export default { addProduct, updateProduct, delProduct, getProduct, getProducts, getProductByType }
