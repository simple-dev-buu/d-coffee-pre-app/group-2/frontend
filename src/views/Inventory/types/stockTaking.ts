import type { User } from '@/views/User/types/User'
import type { Inventory } from './inventory'

export type StockTaking = {
  id?: number
  inventoryId: number
  inventory?: Inventory
  userId: number
  user?: User
  username?: string
  employeeName?: string
  dateCreated: string
  stockTakingItems: StockTakingItem[]
}

export type StockTakingItem = {
  stockTakingId?: number
  inventoryItemId: number
  balanceUpdate: number
  balanceOld: number
}

export const defaultStockTaking: StockTaking = {
  inventoryId: 0,
  userId: 0,
  dateCreated: '',
  stockTakingItems: []
}

export const defaultStockTakingItem: StockTakingItem = {
  inventoryItemId: 0,
  balanceOld: 0,
  balanceUpdate: 0
}
