import type { Branch } from '@/views/Branch/types/branch'
import type { Ingredient } from '@/views/Ingredient/types/ingredient'

export type Inventory = {
  id?: number
  branchId: number
  branch?: Branch
  totalValue: number
  inventoryItems: InventoryItem[]
}

export type InventoryItem = {
  id?: number
  inventoryId: number
  ingredientId: number
  ingredient?: Ingredient
  minBalance: number
  balance: number
  value: number
}

export const defaultInventory: Inventory = {
  branchId: 0,
  totalValue: 0,
  inventoryItems: []
}

export const defaultInventoryItem: InventoryItem = {
  ingredientId: 0,
  inventoryId: 0,
  minBalance: 0,
  balance: 0,
  value: 0
}
