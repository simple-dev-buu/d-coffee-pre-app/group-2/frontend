import type { Ingredient } from '@/views/Ingredient/types/ingredient'
import type { Inventory } from './inventory'
import type { User } from '@/views/User/types/User'

export type ReplenishmentItem = {
  id?: number
  ingredientId: number | undefined
  ingredient?: Ingredient
  balance: number
  cost: number
}

export const defaultReplenishmentItem: ReplenishmentItem = {
  ingredientId: undefined,
  balance: 0,
  cost: 0
}

export type Replenishment = {
  id?: number
  userId: number
  user?: User
  inventoryId: number
  inventory?: Inventory
  createdDate: string
  totalCost: number
  replenishmentItems: ReplenishmentItem[]
}

export const defaultReplenishment: Replenishment = {
  userId: 0,
  inventoryId: 0,
  createdDate: '',
  totalCost: 0,
  replenishmentItems: []
}
