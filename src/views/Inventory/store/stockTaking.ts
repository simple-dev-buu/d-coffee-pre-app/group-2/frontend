import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useInventoryStore } from './inventory'
import { defaultInventoryItem, type Inventory, type InventoryItem } from '../types/inventory'
import { IngredientService } from '@/services/ingredient'
import type { Ingredient } from '@/views/Ingredient/types/ingredient'
import type { StockTaking, StockTakingItem } from '../types/stockTaking'
import { InventoryService } from '@/services/inventory'
import { getDateNowString } from '@/utils/constants'
import { useAuthStore } from '@/views/Authentication/stores/auth'
import { StockTakingService } from '@/services/stockTaking'

export const useStockTakingStore = defineStore('stock-taking', {
  state() {
    return {
      storeInventory: useInventoryStore(),
      auth: useAuthStore(),
      isDialogOpen: ref(false),
      isConfirmDialogOpen: ref(false),
      inventoryItems: ref<InventoryItem[]>([]),
      originalItems: ref<InventoryItem[]>([]),
      stockHistory: ref<StockTaking[]>([]),
      headers: [
        { title: 'Name', key: 'name', sortable: false },
        { title: 'Minimum Balance', key: 'minBalance' },
        { title: 'Balance', key: 'balance' },
        { title: 'Unit', key: 'unit', sortable: false },
        { title: 'Value', key: 'value', sortable: false },
        { title: 'Action', key: 'action', sortable: false }
      ]
    }
  },
  getters: {
    getCurrentInventoryTitle: (state) => state.storeInventory.selectedInventory?.branch?.name,
    getCurrentEmpFullName: (state) => state.auth.getCurrentUser()?.email,
    getTotalNetValue: (state): number =>
      state.inventoryItems.reduce((sum, item) => item.value + sum, 0)
  },
  actions: {
    convertIngIdToUnit(id: number) {
      const ing: Ingredient | undefined = this.storeInventory.availableIngredient.find(
        (item) => item.id === id
      )
      return ing?.unit
    },
    async fetchIngredient() {
      this.storeInventory.availableIngredient = await IngredientService.getAll()
    },
    async fetchStockTakingHistory() {
      this.stockHistory = await StockTakingService.getAll()
    },
    async copyInventoryItems() {
      this.inventoryItems = JSON.parse(JSON.stringify(this.storeInventory.getInventoryItems))
      this.originalItems = [...this.storeInventory.getInventoryItems]
    },
    openDialog() {
      if (this.storeInventory.isBranchSelected()) {
        this.copyInventoryItems()
        this.isDialogOpen = true
      }
    },
    openConfirmDialog() {
      if (this.inventoryItems.length > 0) {
        this.isConfirmDialogOpen = true
      } else {
        alert('Empty items ! Select branch first')
      }
    },
    closeConfirmDialog() {
      this.isConfirmDialogOpen = false
    },
    closeDialog() {
      this.inventoryItems = []
      this.isDialogOpen = false
    },
    addItem() {
      this.inventoryItems.push({
        ...defaultInventoryItem
      })
    },
    removeItem(index: number) {
      this.inventoryItems.splice(index, 1)
    },
    async save() {
      if (this.inventoryItems.length > 0) {
        this.inventoryItems.forEach((item) => {
          item.ingredientId = item.ingredient?.id!
        })
        const inventory: Inventory = {
          id: this.storeInventory.getInventoryId,
          inventoryItems: this.inventoryItems,
          branchId: this.storeInventory.getBranchId!,
          totalValue: this.getTotalNetValue
        }
        await InventoryService.update(inventory)

        const stockTaking: StockTaking = {
          userId: this.auth.getCurrentUser()?.id!,
          inventoryId: inventory.id!,
          dateCreated: getDateNowString(),
          stockTakingItems: inventory.inventoryItems.map((item, index) => {
            return <StockTakingItem>{
              inventoryItemId: item.inventoryId,
              balanceOld: this.originalItems[index].balance ?? 0,
              balanceUpdate: item.balance
            }
          })
        }
        await StockTakingService.create(stockTaking)
      }
      this.storeInventory.reloadInventoryItems()
      this.isConfirmDialogOpen = false
      this.isDialogOpen = false
    }
  }
})
