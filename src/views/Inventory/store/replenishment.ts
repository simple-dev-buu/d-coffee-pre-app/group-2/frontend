import { defineStore } from 'pinia'
import { ref } from 'vue'
import {
  defaultReplenishmentItem,
  type Replenishment,
  type ReplenishmentItem
} from '../types/replenishment'
import type { Ingredient } from '@/views/Ingredient/types/ingredient'
import { IngredientService } from '@/services/ingredient'
import { ReplenishmentService } from '@/services/replenishment'
import { getDateNowString } from '@/utils/constants'
import { useInventoryStore } from './inventory'
import { useAuthStore } from '@/views/Authentication/stores/auth'

export const useReplenishmentStore = defineStore('replenishment', {
  state() {
    return {
      auth: useAuthStore(),
      inventory: useInventoryStore(),
      isDialogOpen: ref(false),
      repItems: ref<ReplenishmentItem[]>([]),
      replenishmentHistory: ref<Replenishment[]>([]),
      availableIngredients: ref<Ingredient[]>([]),
      headers: [
        { title: 'ID', sortable: true },
        { title: 'Ingredient', sortable: false },
        { title: 'Balance', sortable: true },
        { title: 'Cost', sortable: true },
        { title: 'Action', sortable: false }
      ]
    }
  },
  getters: {
    getTotalCost: (state) => state.repItems.reduce((sum, item) => item.cost + sum, 0)
  },
  actions: {
    openDialog() {
      if (this.inventory.isBranchSelected()) {
        this.isDialogOpen = true
      }
    },
    closeDialog() {
      this.repItems = []
      this.isDialogOpen = false
    },
    async save() {
      await ReplenishmentService.create(<Replenishment>{
        createdDate: getDateNowString(),
        userId: this.auth.getCurrentUser()?.id,
        inventoryId: this.inventory.getInventoryId,
        totalCost: this.getTotalCost,
        replenishmentItems: this.repItems
      })
      this.repItems = []
      this.closeDialog()
    },
    removeItem(index: number) {
      this.repItems.splice(index, 1)
    },
    addItem() {
      this.repItems.push({ ...defaultReplenishmentItem })
    },
    async fetchIngredient() {
      this.availableIngredients = await IngredientService.getAll()
    },
    async fetchHistory() {
      this.replenishmentHistory = await ReplenishmentService.getAll()
    }
  }
})
