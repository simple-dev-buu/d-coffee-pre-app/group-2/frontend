import { defineStore } from 'pinia'
import { ref } from 'vue'
import { InventoryService } from '@/services/inventory'
import {
  defaultInventory,
  defaultInventoryItem,
  type Inventory,
  type InventoryItem
} from '../types/inventory'
import type { Branch } from '@/views/Branch/types/branch'
import { BranchService } from '@/services/branch'
import type { Ingredient } from '@/views/Ingredient/types/ingredient'
import { IngredientService } from '@/services/ingredient'

export const useInventoryStore = defineStore('inventory', {
  state() {
    return {
      service: InventoryService,
      availableInventory: ref<Inventory[]>([]),
      availableBranch: ref<Branch[]>([]),
      availableIngredient: ref<Ingredient[]>([]),
      selectedInventory: ref<Inventory>(),
      tempItem: ref<InventoryItem>({ ...defaultInventoryItem }),
      tempInventory: ref<Inventory>({ ...defaultInventory }),
      inventoryItems: ref<InventoryItem[]>([]),
      titleDialog: ref(''),
      isDialogOpen: ref(false),
      headers: [
        { title: 'ID', key: 'id' },
        { title: 'Name', key: 'name', sortable: false },
        { title: 'Minimum Balance', key: 'minBalance' },
        { title: 'Balance', key: 'balance' },
        { title: 'Unit', key: 'unit', sortable: false },
        { title: 'Value', key: 'status', sortable: false },
        { title: 'Status', key: 'status', sortable: false }
      ],
      printDialogState() {}
    }
  },
  getters: {
    getInventoryId: (state) => state.selectedInventory?.id,
    getBranchId: (state) => state.selectedInventory?.branchId,
    getInventoryItems: (state) => state.inventoryItems
  },
  actions: {
    isLowStock(minimum: number, balance: number) {
      if (balance >= minimum) {
        return false
      } else {
        return true
      }
    },
    statusTextChecking(minimum: number, balance: number) {
      if (balance >= minimum) {
        return 'OK'
      } else {
        return 'Low'
      }
    },
    convertIngIdToUnit(id: number) {
      const ing: Ingredient | undefined = this.availableIngredient.find((item) => item.id === id)
      return ing?.unit
    },
    async fetchAvailableInventory() {
      this.availableInventory = await this.service.getAll()
    },
    async fetchInventoryItems() {
      if (this.selectedInventory) {
        this.selectedInventory = await this.service.getOne(this.getInventoryId!)
      }
    },
    async fetchIngredient() {
      this.availableIngredient = await IngredientService.getAll()
    },
    handleInventorySelected(value: any) {
      this.selectedInventory = value
      this.reloadInventoryItems()
    },
    async reloadInventoryItems() {
      const inv: Inventory = await this.service.getOne(this.selectedInventory?.id!)
      this.inventoryItems = inv.inventoryItems
    },
    openDialog(item?: Inventory) {
      if (item) {
        this.titleDialog = 'Edit Inventory'
        this.tempInventory = { ...item }
      } else {
        this.titleDialog = 'New Inventory'
      }
      this.isDialogOpen = true
    },
    closeDialog() {
      this.resetItem()
      this.isDialogOpen = false
    },
    async fetchAll() {
      this.availableInventory = await InventoryService.getAll()
      this.availableBranch = await BranchService.getAll()
    },
    deleteItem(index: number) {
      if (confirm('Are you sure to delete ?')) {
        this.service.delete(index)
        this.resetItem()
      }
      this.fetchAll()
    },
    resetItem() {
      this.tempItem = { ...defaultInventoryItem }
      this.tempInventory = { ...defaultInventory }
    },
    async save() {
      if (this.tempInventory.id) {
        await this.service.update(this.tempInventory)
      } else {
        await this.service.create(this.tempInventory)
      }
      this.fetchAll()
      this.closeDialog()
    },
    isBranchSelected(): boolean {
      if (this.selectedInventory) {
        return true
      } else {
        alert('Select branch first !')
        return false
      }
    },
    async deleteInventory() {
      if (this.isBranchSelected()) {
        if (confirm('Are you sure to dismantle inventory ?')) {
          await this.service.delete(this.selectedInventory?.id!)
        }
      }
    }
  }
})
