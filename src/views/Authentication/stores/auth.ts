import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/views/User/types/User'
import { useMessageStore } from '../stores/message'
import { useRouter } from 'vue-router'
import authService from '../services/auth'

export const useAuthStore = defineStore('auth', () => {
  const router = useRouter()
  const messageStore = useMessageStore()
  const currentUser = ref<User | null>(null)
  const users = ref<User[]>([
    {
      id: 1,
      email: 'admin@mail.com',
      name: 'แอดมิน สุดหล่อ',
      password: '1234',
      roles: 'admin'
    }
  ])

  const login = async (email: string, password: string) => {
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', JSON.stringify(res.data.access_token))
      messageStore.showSnackbar('Login success')
      router.push('/')
    } catch (e: any) {
      messageStore.showSnackbar('Login failed')
    }
  }
  const logout = () => {
    currentUser.value = null
    router.push('/login')
    if (localStorage.getItem('access_token')) {
      localStorage.removeItem('user')
      localStorage.removeItem('access_token')
      messageStore.showSnackbar('Logout success')
    }
  }

  // function findUserByEmail(email: string, password: string) {
  //   const user = users.value.find((user) => user.email === email)
  //   if (user && user.password === password) {
  //     return user
  //   } else {
  //     throw new Error('Invalid email or password')
  //   }
  // }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }
  function getToken(): User | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }
  return { login, currentUser, users, getCurrentUser, logout, getToken }
})
