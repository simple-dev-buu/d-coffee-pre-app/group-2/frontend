import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useMessageStore = defineStore('message', () => {
  const snackbar = ref(false)
  const text = ref('')

  const showSnackbar = (msg: string) => {
    text.value = msg
    snackbar.value = true
  }

  return { snackbar, text, showSnackbar }
})
