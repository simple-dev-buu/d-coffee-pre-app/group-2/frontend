type Gender = 'male' | 'female' | 'others'
type Employee = {
  id?: number
  role: string
  firstName: string
  lastName: string
  tel: string
  gender: 'male' | 'female' | 'others'
  birthDate: string
  qualification: string
  moneyRate: number
  minWork: number
  startDate: string
}

export type { Gender, Employee }
