import http from '@/services/http'
import type { Employee } from '../types/Employee'

function addEmployee(employee: Employee) {
  return http.post('/employees', employee)
}

function updateEmployee(employee: Employee) {
  return http.patch(`/employees/${employee.id}`, employee)
}

function delEmployee(employee: Employee) {
  return http.delete(`/employees/${employee.id}`)
}

function getEmployee(id: number) {
  return http.get(`/employees/${id}`)
}

function getAllEmployee() {
  return http.get('/employees')
}

export default { addEmployee, updateEmployee, delEmployee, getEmployee, getAllEmployee }
