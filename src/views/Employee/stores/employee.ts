import { nextTick, ref } from 'vue'
import type { Employee } from '../types/Employee'
import { defineStore } from 'pinia'
import employeeService from '@/views/Employee/services/employee'
import type { VForm } from 'vuetify/components'

export const useEmployeeStore = defineStore('employee', () => {
  //const loadingStore = useLoadingStore()
  const employees = ref<Employee[]>([])
  const initialEmployee: Employee = {
    role: 'พนักงาน',
    firstName: '',
    lastName: '',
    tel: '',
    gender: 'male',
    birthDate: '00/00/0000',
    qualification: '',
    moneyRate: 0,
    minWork: 0,
    startDate: '00/00/0000'
  }
  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initialEmployee)))

  // Data Function

  async function getEmployee(id: number) {
    //loadingStore.doLoad()
    const respond = await employeeService.getEmployee(id)
    editedEmployee.value = respond.data
    //loadingStore.finish()
  }

  async function getAllEmployee() {
    //loadingStore.doLoad()
    const respond = await employeeService.getAllEmployee()
    employees.value = respond.data
    //loadingStore.finish()
  }

  // ADD and UPDATE
  async function saveEmployee() {
    //loadingStore.doLoad()
    const employee = editedEmployee.value
    if (!employee.id) {
      // Add new  ถ้าไม่มี id
      console.log('Post' + JSON.stringify(employee))
      const respond = await employeeService.addEmployee(employee)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(employee))
      const respond = await employeeService.updateEmployee(employee)
    }

    await getAllEmployee()
    //loadingStore.finish()
  }

  //DELETE
  async function deleteEmployee() {
    //loadingStore.doLoad()
    const user = editedEmployee.value
    const respond = await employeeService.delEmployee(user)

    await getAllEmployee()
    //loadingStore.finish()
  }

  function clearForm() {
    editedEmployee.value = JSON.parse(JSON.stringify(initialEmployee))
  }

  const header = [
    { title: 'ID', key: 'id', sortable: true },
    { title: 'Role', key: 'role', sortable: true },
    { title: 'FirstName', key: 'firstName', sortable: false },
    { title: 'LastName', key: 'lastName', sortable: false },
    { title: 'Tel.', key: 'tel', sortable: false },
    { title: 'Gender', key: 'gender', sortable: false },
    { title: 'Birth Date', key: 'birthDate', sortable: false },
    { title: 'Qualification', key: 'qualification', sortable: false },
    { title: 'MoneyRate', key: 'moneyRate', sortable: false },
    { title: 'MinWork', key: 'minWork', sortable: false },
    { title: 'Start Date', key: 'startDate', sortable: false }
  ]

  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)

  let editedIndex = -1
  //let lastId = 4

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      clearForm()
    })
  }
  async function deleteItemConfirm() {
    // Delete item from list
    await deleteEmployee()
    closeDelete()
  }

  async function editItem(item: Employee) {
    if (!item.id) return
    await getEmployee(item.id)
    dialog.value = true
  }

  async function deleteItem(item: Employee) {
    if (!item.id) return
    await getEmployee(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  const openDialog = () => {
    dialog.value = true
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }
  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if (!valid) return

    await saveEmployee()
    closeDialog()
  }

  function onSubmit() {}

  return {
    employees,
    getAllEmployee,
    saveEmployee,
    deleteEmployee,
    editedEmployee,
    getEmployee,
    clearForm,
    header,
    closeDelete,
    closeDialog,
    deleteItem,
    deleteItemConfirm,
    editItem,
    save,
    dialog,
    loading,
    form,
    onSubmit,
    dialogDelete,
    openDialog
  }
})
