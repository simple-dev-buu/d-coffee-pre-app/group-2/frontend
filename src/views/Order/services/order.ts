import type { Receipt } from '@/views/Pos/types/Receipt'
import type { ReceiptItem } from '@/views/Pos/types/ReceiptItem'
import http from '@/services/http'

type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
  }[]
  userId: number
}

function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0
  }
  receiptDto.userId = receipt.userId
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })

  return http.post('/orders', receiptDto)
}

export default { addOrder }
