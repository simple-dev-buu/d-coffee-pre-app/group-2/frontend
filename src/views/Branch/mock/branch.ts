import type { Branch } from '../types/branch'

export const mockBranch: Branch[] = [
  {
    id: 1,
    name: 'BUU',
    location: 'Chonburi, 20130'
  },
  {
    id: 2,
    name: 'Pattaya',
    location: 'Chonburi, 20150'
  },
  {
    id: 3,
    name: 'Sattaheap',
    location: 'Chonburi, 20180'
  }
]
