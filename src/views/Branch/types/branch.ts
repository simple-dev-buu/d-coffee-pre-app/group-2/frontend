export type Branch = {
  id?: number
  name: string
  location: string
}

export const defaultBranch: Branch = {
  name: '',
  location: ''
}
