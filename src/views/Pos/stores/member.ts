import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '../types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    { id: 1, name: 'NA', tel: '011111112' },
    { id: 2, name: 'NB', tel: '011111113' }
  ])
  const getMemberByTel = (tel: string): Member | null => {
    for (const member of members.value) {
      if (member.tel === tel) return member
    }
    return null
  }
  return { members, getMemberByTel }
})
