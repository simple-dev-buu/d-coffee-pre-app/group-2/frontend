import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/views/Product/types/Product'
import ProductService from '@/views/Product/services/product'
// import productService from '@/Product/services/product'

export const usePosStore = defineStore('pos', () => {
  const productService = new ProductService()
  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])

  async function getProducts() {
    try {
      //   loadingStore.doLoad()
      let res = await productService.getProductByType(1)
      products1.value = res.data
      res = await productService.getProductByType(2)
      products2.value = res.data
      res = await productService.getProductByType(3)
      products3.value = res.data
      //   loadingStore.finish()
    } catch (e) {
      console.log('Error')
      //   loadingStore.finish()
    }
  }

  return { products1, products2, products3, getProducts }
})
