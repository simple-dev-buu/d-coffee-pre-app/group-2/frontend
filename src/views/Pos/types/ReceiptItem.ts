import type { Product } from '@/views/Product/types/Product'

const defaultReceiptItem = {
  id: -1,
  name: '',
  price: 0,
  unit: 0,
  productId: -1,
  product: null
}
type ReceiptItem = {
  id: number
  name: string
  price: number
  unit: number
  productId: number
  product?: Product
}

export { type ReceiptItem, defaultReceiptItem }
