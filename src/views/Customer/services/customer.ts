import http from '@/services/http'
import type { Customer } from '../types/Customer'

function addCustomer(employee: Customer) {
  return http.post('/customers', employee)
}

function updateCustomer(employee: Customer) {
  return http.patch(`/customers/${employee.id}`, employee)
}

function delCustomer(employee: Customer) {
  return http.delete(`/customers/${employee.id}`)
}

function getCustomer(id: number) {
  return http.get(`/customers/${id}`)
}

function getAllCustomer() {
  return http.get('/customers')
}

export default { addCustomer, updateCustomer, delCustomer, getCustomer, getAllCustomer }
