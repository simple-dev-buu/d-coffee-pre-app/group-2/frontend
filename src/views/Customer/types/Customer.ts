type Customer = {
  id?: number
  firstName: string
  lastName: string
  tel: string
  point: number
  birthDate: string
  registerDate: string
}

export type { Customer }
