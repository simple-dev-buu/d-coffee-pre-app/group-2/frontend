import { nextTick, ref } from 'vue'
import type { Customer } from '../types/Customer'
import { defineStore } from 'pinia'
import customerService from '@/views/Customer/services/customer'
import type { VForm } from 'vuetify/components'

export const useCustomerStore = defineStore('customer', () => {
  //const loadingStore = useLoadingStore()
  const customers = ref<Customer[]>([])
  const initialCustomer: Customer = {
    firstName: '',
    lastName: '',
    tel: '',
    point: 0,
    birthDate: '',
    registerDate: ''
  }
  const editedCustomer = ref<Customer>(JSON.parse(JSON.stringify(initialCustomer)))

  // Data Function

  async function getCustomer(id: number) {
    //loadingStore.doLoad()
    const respond = await customerService.getCustomer(id)
    editedCustomer.value = respond.data
    //loadingStore.finish()
  }

  async function getAllCustomer() {
    //loadingStore.doLoad()
    const respond = await customerService.getAllCustomer()
    customers.value = respond.data
    //loadingStore.finish()
  }

  // ADD and UPDATE
  async function saveCustomer() {
    //loadingStore.doLoad()
    const customer = editedCustomer.value
    if (!customer.id) {
      // Add new  ถ้าไม่มี id
      console.log('Post' + JSON.stringify(customer))
      const respond = await customerService.addCustomer(customer)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(customer))
      const respond = await customerService.updateCustomer(customer)
    }

    await getAllCustomer()
    //loadingStore.finish()
  }

  //DELETE
  async function deleteCustomer() {
    //loadingStore.doLoad()
    const user = editedCustomer.value
    const respond = await customerService.delCustomer(user)

    await getAllCustomer()
    //loadingStore.finish()
  }

  function clearForm() {
    editedCustomer.value = JSON.parse(JSON.stringify(initialCustomer))
  }

  const header = [
    { title: 'ID', key: 'id', sortable: true },
    { title: 'FirstName', key: 'firstName', sortable: false },
    { title: 'LastName', key: 'lastName', sortable: false },
    { title: 'Tel.', key: 'tel', sortable: false },
    { title: 'Point', key: 'point', sortable: false },
    { title: 'Birth Date', key: 'birthDate', sortable: false },
    { title: 'Register Date', key: 'registerDate', sortable: false }
  ]

  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)

  let editedIndex = -1
  //let lastId = 4

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      clearForm()
    })
  }
  async function deleteItemConfirm() {
    // Delete item from list
    await deleteCustomer()
    closeDelete()
  }

  async function editItem(item: Customer) {
    if (!item.id) return
    await getCustomer(item.id)
    dialog.value = true
  }

  async function deleteItem(item: Customer) {
    if (!item.id) return
    await getCustomer(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  const openDialog = () => {
    dialog.value = true
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }
  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if (!valid) return

    await saveCustomer()
    closeDialog()
  }

  function onSubmit() {}

  return {
    customers,
    getAllCustomer,
    saveCustomer,
    deleteCustomer,
    editedCustomer,
    getCustomer,
    clearForm,
    header,
    closeDelete,
    closeDialog,
    deleteItem,
    deleteItemConfirm,
    editItem,
    save,
    dialog,
    loading,
    form,
    onSubmit,
    dialogDelete,
    openDialog
  }
})
