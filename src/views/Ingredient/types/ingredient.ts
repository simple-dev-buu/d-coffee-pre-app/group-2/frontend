export type Ingredient = {
  id?: number
  name: string
  unit: string
}

export const defaultIngredient: Ingredient = {
  name: '',
  unit: ''
}
