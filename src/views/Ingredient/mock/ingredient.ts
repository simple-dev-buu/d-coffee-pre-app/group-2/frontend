import type { Ingredient } from '../types/ingredient'

export const mockIngredient: Ingredient[] = [
  {
    id: 1,
    name: 'Coffee Beans',
    unit: 'Kg'
  },
  {
    id: 2,
    name: 'Cow Milk',
    unit: 'L'
  },
  {
    id: 3,
    name: 'Almond Milk',
    unit: 'L'
  }
]
