import type { User } from '../types/User'
import http from '@/services/http'

async function save(item: User) {
  const res = await http.post('/users', item)
  return res.data
}
async function update(item: User) {
  const res = await http.patch(`/users/${item.id}`, item)
  return res.data
}
async function getAll() {
  const res = await http.get('/users')
  return res.data
}
async function getUserByEmail(email: String) {
  return http.get(`/users/${email}`)
}
async function getUser(id: number) {
  return http.get(`/users/${id}`)
}
async function deleteUser(id: number) {
  await http.delete(`/users/${id}`)
}

export default {
  save,
  update,
  getAll,
  getUserByEmail,
  getUser,
  deleteUser
}
