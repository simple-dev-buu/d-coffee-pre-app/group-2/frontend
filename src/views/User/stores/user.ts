import { defineStore } from 'pinia'
// import { useLoadingStore } from './loading'
import userService from '../services/user'
import { ref } from 'vue'
import type { User } from '../types/User'
import type { Role } from '@/views/Pos/types/User'

export const useUserStore = defineStore('user', () => {
  // const loadingStore = useLoadingStore()
  //value: (roles: []) => roles.map((role: Role) => `${role.name}`)
  const items = ref<User[]>([])
  const dialog = ref(false)
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Email', key: 'email', sortable: false },
    { title: 'Roles', key: 'roles[0].name', sortable: false },
    { title: 'Action', key: 'action', sortable: false }
  ]
  const initialUser: User = {
    name: '',
    email: '',
    password: '',
    roles: 'user'
  }
  const userItem = ref<User>(JSON.parse(JSON.stringify(initialUser)))

  const currentUser = ref<User | null>()

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  // const save = async () => {
  //   if (productItem.id) {
  //     await productService.update(productItem)
  //     console.log(productItem)
  //   } else {
  //     await productService.save(productItem)
  //     console.log(productItem)
  //   }
  //   closeDialog()
  //   getAll()
  // }

  async function getUser(id: number) {
    try {
      // loadingStore.doLoad()
      const res = await userService.getUser(id)
      userItem.value = res.data
      userItem.value.roles = res.data.roles.name
      // loadingStore.finish()
    } catch (e: any) {
      // loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  async function save() {
    try {
      // loadingStore.doLoad()
      const user = userItem.value
      console.log(user)
      if (!user.id) {
        // Add new
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.save(user)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.update(user)
      }
      await getAll()
      closeDialog()
      // loadingStore.finish()
    } catch (e: any) {
      // messageStore.showMessage(e.message)
      // loadingStore.finish()
    }
  }
  const resetItem = () => {
    userItem.value = JSON.parse(JSON.stringify(initialUser))
  }
  const getAll = async () => {
    const res = await userService.getAll()
    items.value = res
    console.log(items.value)
  }
  const editItem = async (item: User) => {
    // product.id = item.id
    // productItem = item.name
    // productItem.price = item.price
    // productItem.type = item.type
    // console.log(productItem.type)
    // openDialog()

    if (item.id) {
      //   await productService.getProduct(item.id)
      //   openDialog()
      //   console.log(getProduct(item.id))
    }
  }

  const deleteItem = async (item: User) => {
    if (item.id) {
      await userService.deleteUser(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    userItem,
    dialog,
    currentUser,
    editItem,
    deleteItem,
    getAll,
    getUser,
    openDialog,
    closeDialog,
    save,
    resetItem
  }
})
