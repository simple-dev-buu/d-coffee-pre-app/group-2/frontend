// import type { Role } from "@/views/Role/types/Role"
// type Role = 'admin' | 'user'

type User = {
  id?: number
  name: string
  email: string
  password: string
  roles: string
}

export type { User }
