import axios from 'axios'

const baseURL = 'http://localhost:3000'

const token = localStorage.getItem('token')

const instance = axios.create({
  baseURL: baseURL,
  headers: { Authorization: `Bearer ${token}` }
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (err) => {
    throw new Error()
  }
)

instance.interceptors.response.use(
  (res) => {
    return res
  },
  (err) => {
    throw new Error()
  }
)

export default instance
