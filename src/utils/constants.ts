export type SortItem = {
  key: string
  order?: boolean | 'asc' | 'desc'
}

export const requiredRule = (v: any) => !!v || 'Field is required'

export const ruleFiles = (files: File[]) =>
  !files || !files.some((file) => file.size > 1e6) || 'Image size should be less than 1 Mb!'

export function getDateNowString(): string {
  return new Date().toISOString().slice(0, 10)
}

export function getTimeNowString(): string {
  return new Date().toLocaleTimeString('th-TH', { hour12: false })
}
